#include "common.h"
#include "nicegraf.h"
#include "nicegraf_util.h"
#include <GLFW/glfw3.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct shadered_uniforms {
  float screen_size[2];
  float time;
};

ngf_graphics_pipeline *pipeline = NULL;
ngf_graphics_program *program = NULL;
ngf_buffer *uniform_buffer = NULL;
ngf_pipeline_layout *layout = NULL;
ngf_draw_op *draw_op = NULL;
ngf_descriptor_set *ubo_set = NULL;

const char *SHADERED_VERT_SHADER = R"(#version 430
precision mediump float;
void main() {
  vec2 p[] = vec2[](
    vec2(-1.0, -1.0),
    vec2( 3.0, -1.0),
    vec2(-1.0,  3.0)
  );
  gl_Position = vec4(p[gl_VertexID % 3], 0.0, 1.0);
}
)";

void reload_program(const char *frag_file_name) {
  printf("Reload start\n");
  FILE *frag_file = fopen(frag_file_name, "r");
  assert(frag_file != NULL);
  fseek(frag_file, 0, SEEK_END);
  size_t size = ftell(frag_file);
  fseek(frag_file, 0, SEEK_SET);
  char *frag_code = (char*)malloc(size);
  fread(frag_code, 1, size, frag_file);
  fclose(frag_file);
  ngf_graphics_program_info shader_config;
  shader_config.nstages = 2;
  shader_config.stages[0].type = NGF_STAGE_VERTEX;
  shader_config.stages[0].content = SHADERED_VERT_SHADER;
  shader_config.stages[0].content_length = strlen(SHADERED_VERT_SHADER);
  shader_config.stages[1].type = NGF_STAGE_FRAGMENT;
  shader_config.stages[1].content = frag_code;
  shader_config.stages[1].content_length = size;
  ngf_error err = ngf_build_graphics_programs(&shader_config, 1, &program);
  assert(err == NGF_ERROR_OK);
  free(frag_code);
  printf("Reload end\n");
}

void recreate_pipelines(int w, int h) {
  if(pipeline != nullptr) {
    ngf_destroy_graphics_pipeline(pipeline);
  }
  if (draw_op != nullptr) {
    ngf_destroy_draw_op(draw_op);
  }
  ngf_irect2d window_rect = {0, 0, w, h};
  ngf_util_graphics_pipeline_data pipeline_data;
  ngf_util_create_default_graphics_pipeline_data(
    program,
    layout,
    &window_rect,
    &pipeline_data);
  ngf_error err;
  printf("Creating pp\n");
  err = ngf_create_graphics_pipeline(&pipeline_data.pipeline_info, &pipeline);
  printf("Created\n");
  assert(err == NGF_ERROR_OK);

  // Draw call configuration.
  ngf_descriptor_set_binding ubo_set_binding = {
    .set = ubo_set,
    .slot = 0
  };
  ngf_draw_subop_info draw_subop_config = {
    .dynamic_state_cmds = nullptr,
    .ndynamic_state_cmds = 0,
    .ndescriptor_set_bindings = 1,
    .descriptor_set_bindings = &ubo_set_binding,
    .vertex_buffer_bindings = nullptr,
    .nvertex_buffer_bindings = 0,
    .mode = NGF_DRAW_MODE_DIRECT
  }; // ????
  draw_subop_config.primitive = NGF_PRIMITIVE_TYPE_TRIANGLE_LIST;
  draw_subop_config.first_element = 0U;
  draw_subop_config.nelements = 3U;
  draw_subop_config.ninstances = 1U;
  ngf_draw_op_info draw_op_config = {
    .pipeline = pipeline,
    .subops = &draw_subop_config,
    .nsubops = 1
  };
  err = ngf_create_draw_op(&draw_op_config, &draw_op);
  assert(err == NGF_ERROR_OK);
}

void on_window_resize(GLFWwindow *win, int w, int h) {
  glfwGetWindowSize(win, &w, &h);
  recreate_pipelines(w, h);
}

void on_key(GLFWwindow *win, int key, int scc, int action, int mods) {
  if (key == GLFW_KEY_R && action == GLFW_PRESS) {
    printf("Key triggered\n");
    reload_program("shadered.glsl");
    on_window_resize(win, 0, 0);
  }
}

void main_loop(GLFWwindow *win, ngf_common_context_and_swapchain c) {
  ngf_error err = NGF_ERROR_OK;
  
  err = ngf_set_context(c.ctx, c.sc);
  assert(err == NGF_ERROR_OK);

  // Compile program
  reload_program("shadered.glsl");
  
  // Create UBO
  ngf_buffer_info buffer_info = {
    .size = sizeof(shadered_uniforms),
    .type = NGF_BUFFER_TYPE_UNIFORM,
    .access_freq = NGF_BUFFER_USAGE_STREAM,
    .access_type = NGF_BUFFER_ACCESS_DRAW
  };
  err = ngf_create_buffer(&buffer_info, &uniform_buffer);
  assert(err == NGF_ERROR_OK);

  // Create descriptor sets.
  ngf_descriptor_info ubo_descriptor_info = {
    .type = NGF_DESCRIPTOR_UNIFORM_BUFFER,
    .id = 0
  };
  ngf_descriptors_layout_info ubo_set_layout_info = {
    .descriptors = &ubo_descriptor_info,
    .ndescriptors = 1
  };
  ngf_descriptors_layout *ubo_set_layout;
  err = ngf_create_descriptors_layout(&ubo_set_layout_info, &ubo_set_layout);
  assert(err == NGF_ERROR_OK);
  err = ngf_create_descriptor_set(ubo_set_layout, &ubo_set);
  assert(err == NGF_ERROR_OK);

  // Create pipeline layout.
  ngf_pipeline_layout_info layout_info = {
    .ndescriptors_layouts = 1,
    .descriptors_layouts = &ubo_set_layout
  };
  err = ngf_create_pipeline_layout(&layout_info, &layout);
  assert(err == NGF_ERROR_OK);

  // Write to descriptor set.
  ngf_descriptor_write_buffer buffer_bind = {
    .buffer = uniform_buffer,
    .offset = 0,
    .range = sizeof(shadered_uniforms)

  };
  ngf_descriptor_write ubo_set_write = {
    .index = 0,
    .type = NGF_DESCRIPTOR_UNIFORM_BUFFER
  };
  ubo_set_write.op.buffer_bind = buffer_bind;
  err = ngf_apply_descriptor_writes(&ubo_set_write, 1, ubo_set);
  assert(err == NGF_ERROR_OK);

  // Force pipeline creation.
  on_window_resize(win, 0, 0);

  // Create render pass
  ngf_attachment_load_op loadop = NGF_LOAD_OP_CLEAR;
  ngf_clear_info clear = {
    .clear_color = { 0.0f, 0.0f, 0.0f, 0.0f },
    .clear_depth = 1.0f,
    .clear_stencil = 0
  };
  ngf_pass_info pass_info = {
    .clears = &clear,
    .loadops = &loadop,
    .nloadops = 1
  };
  ngf_pass *pass;
  err = ngf_create_pass(&pass_info, &pass);
  assert(err == NGF_ERROR_OK);

  // Acquire default rt
  ngf_render_target *rt;
  ngf_default_render_target(&rt);
  
  glfwSetKeyCallback(win, on_key);
  while (!glfwWindowShouldClose(win)) {
    glfwPollEvents();
    int w, h;
    glfwGetWindowSize(win, &w, &h);
    shadered_uniforms u = {
      .screen_size = {(float)w, (float)h},
      .time = (float)glfwGetTime()
    };
    err = ngf_populate_buffer(uniform_buffer, 0, sizeof(u), &u);
    assert(err == NGF_ERROR_OK);
    ngf_execute_pass(pass, rt, draw_op, 1);
    ngf_present(c.sc);
  }
}

int main(int argc, char *argv[]) {
  ngf_common_start_app("Shader editor", argc, argv);
  return 0;
}

