#include "common.h"
#include "nicegraf.h"
#include "nicegraf_util.h"
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

ngf_context *ctx = nullptr;
ngf_graphics_pipeline *pipeline = nullptr;
ngf_draw_op *draw_op = nullptr;
ngf_buffer *vertex_buffer = nullptr;

void recreate_pipelines(size_t win_size_x, size_t win_size_y) {}

void main_loop(GLFWwindow *win, ngf_context *c) {
  int w, h;
  glfwGetWindowSize(win, &w, &h);
  ngf_error err = ngf_set_context(c);
  assert(err == NGF_ERROR_OK);
  while (!glfwWindowShouldClose(win)) {
    ngf_begin_frame(c);
    glfwPollEvents();
    //ngf_execute_pass(pass, rt, draw_op, 1);
    ngf_end_frame(c);
  }
}

void on_window_resize(GLFWwindow *win, int w, int h) {
  glfwGetWindowSize(win, &w, &h);
  recreate_pipelines(w, h);
}

int main(int argc, char *argv[]) {
  ngf_common_start_app("Triangle", argc, argv);
  return 0;
}
