#include "nicegraf.h"
#include "nicegraf_util.h"
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if defined(_WIN32)
#define filelen(f) _filelength(_fileno(f))
#elif defined(_WIN64)
#define filelen(f) _filelength64(_fileno(f))
#else
int fileno(FILE *f);
#include <sys/stat.h>
size_t filelen(FILE *f) {
  struct stat statbuf;
  fstat(fileno(f), &statbuf);
  return statbuf.st_size;
}
#endif

typedef struct {
  ngf_shader_stage_info stage_info;
  uint32_t *layout_metadata;
} shader_module;

void load_shader_module(const char *shader_name,
                        const char *shader_layout_name,
                        shader_module *sm);
typedef struct {
  float time;
} uniform_data;

void debug_callback(const char *msg, const void *userdata) {
  fprintf(stderr, "%s\n", msg);
}

/** This state will be recreated on every window resize. */
typedef struct {
  ngf_graphics_pipeline *pipeline;
  ngf_draw_op  *draw_op;
} transient_state;

transient_state create_transient_state(ngf_pipeline_layout *pipeline_layout,
                                       ngf_descriptor_set *descriptor_set,
                                       uint32_t width,
                                       uint32_t height,
                                       ngf_shader_stage *vert_stage,
                                       ngf_shader_stage *frag_stage,
                                       ngf_vertex_buf_bind_op *vert_binding);

void create_simple_shader_stages(const ngf_shader_stage_info *vert,
                                 const ngf_shader_stage_info *frag,
                                 ngf_shader_stage **output);

int main(int argc, char *argv[]) {
  // Load shader data.
  shader_module shader_modules[4];
  load_shader_module("big_sample_data/composition.gl430.vert",
                     "big_sample_data/composition.rlo.vert",
                     &shader_modules[0]);
  load_shader_module("big_sample_data/composition.gl430.frag",
                     "big_sample_data/composition.rlo.frag",
                     &shader_modules[1]);
  load_shader_module("big_sample_data/triangle.gl430.vert",
                     "big_sample_data/triangle.rlo.vert",
                     &shader_modules[2]);
  load_shader_module("big_sample_data/triangle.gl430.frag",
                     "big_sample_data/triangle.rlo.frag",
                     &shader_modules[3]);

  // Create a GLFW window.
  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); // Make sure GLFW doesn't try
                                                // to create a context for us.
  GLFWwindow *win = glfwCreateWindow(800, 600, "Nicegraf", NULL, NULL);
  assert(win);
  
  // Initialize Nicegraf.
  ngf_error ngf_err = ngf_initialize(NGF_DEVICE_PREFERENCE_DONTCARE);
  assert(ngf_err == NGF_ERROR_OK);

  // Create a Nicegraf context.
  ngf_context *ctx;
  ngf_swapchain_info swapchain_info = {
    .cfmt = NGF_IMAGE_FORMAT_BGRA8,
    .dfmt = NGF_IMAGE_FORMAT_UNDEFINED,
    .nsamples = 16, // 16x MSAA
    .capacity_hint = 2, // Double-buffered
    .width = 800,
    .height = 600,
    .native_handle = NGF_GLFW_GET_HANDLE(win)
  };
  ngf_context_info ctx_info = {
    .swapchain_info = &swapchain_info,
    .shared_context = NULL
  };
  ngf_err = ngf_create_context(&ctx_info, &ctx);
  assert(ngf_err == NGF_ERROR_OK);
  glfwSetWindowUserPointer(win, ctx);

  // Activate context.
  ngf_set_context(ctx);
  ngf_debug_message_callback(NULL, debug_callback);

  // Create shader stages.
  ngf_shader_stage *shader_stages[] = {NULL, NULL};
  create_simple_shader_stages(&shader_modules[0].stage_info,
                              &shader_modules[1].stage_info,
                              shader_stages);

  // Create render pass.
  ngf_clear_info pass_clear_info = {
    .clear_color = {0.0f, 0.0f, 0.0f, 0.0f},
    .clear_depth = 1.0f,
    .clear_stencil = 0u
  };
  const ngf_attachment_load_op pass_load_op = NGF_LOAD_OP_CLEAR;
  ngf_pass_info pass_info = {
    .clears = &pass_clear_info,
    .loadops = &pass_load_op,
    .nloadops = 1
  };
  ngf_pass *pass = NULL;
  ngf_err = ngf_create_pass(&pass_info, &pass);

  // Create and populate the vertex data buffer.
  ngf_buffer_info vertex_buffer_info = {
    .size = 6 * sizeof(float) * 2,
    .type = NGF_BUFFER_TYPE_VERTEX,
    .access_freq = NGF_BUFFER_USAGE_STATIC,
    .access_type = NGF_BUFFER_ACCESS_DRAW,
  };
  ngf_buffer *vertex_buffer;
  ngf_err = ngf_create_buffer(&vertex_buffer_info, &vertex_buffer);
  assert(ngf_err == NGF_ERROR_OK);
  float buffer_data[] = {
    1.0, 1.0,
    0.0, 1.0,
    0.0, 0.0,
    1.0, 1.0,
    0.0, 0.0,
    1.0, 0.0,
  };
  ngf_err = ngf_populate_buffer(vertex_buffer, 0, sizeof(buffer_data),
                                buffer_data);
  assert(ngf_err == NGF_ERROR_OK);

  // Create the uniform data buffer.
  ngf_buffer_info uniform_buffer_info = {
    .size = sizeof(uniform_data), 
    .type = NGF_BUFFER_TYPE_UNIFORM,
    .access_freq = NGF_BUFFER_USAGE_DYNAMIC,
    .access_type = NGF_BUFFER_ACCESS_DRAW,
  };
  ngf_buffer *uniform_buffer = NULL;
  ngf_err = ngf_create_buffer(&uniform_buffer_info, &uniform_buffer);
  assert(ngf_err == NGF_ERROR_OK);
 
  // Load image data.
  FILE *image = fopen("LENA.DATA", "rb");
  assert(image != NULL);
  fseek(image, 0, SEEK_END);
  const uint32_t image_data_size = ftell(image);
  fseek(image, 0, SEEK_SET);
  uint8_t *image_data = malloc(image_data_size);
  fread(image_data, 1, image_data_size, image);

  // Create and populate a texture.
  ngf_image_info image_info = {
    .type = NGF_IMAGE_TYPE_IMAGE_2D,
    .extent = {
      .width = 512u,
      .height = 512u,
      .depth = 1u
    },
    .nmips = 1u,
    .format = NGF_IMAGE_FORMAT_RGBA8,
    .nsamples = 0u,
    .usage_hint = NGF_IMAGE_USAGE_SAMPLE_FROM
  };
  ngf_image *texture = NULL;
  ngf_err = ngf_create_image(&image_info, &texture);
  assert(ngf_err == NGF_ERROR_OK);
  ngf_offset3d off = {
    .x = 0, .y = 0, .z = 0
  };
  ngf_extent3d ext = {
    .width = 512, .height = 512, .depth = 1
  };
  ngf_err = ngf_populate_image(texture, 0, off, ext, image_data);
  assert(ngf_err == NGF_ERROR_OK);
  free(image_data);

  // Create a texture for off-screen rendering.
  ngf_image_info offscreen_rt_texture_info = {
    .type = NGF_IMAGE_TYPE_IMAGE_2D,
    .extent = {
      .width = 512,
      .height = 512,
      .depth = 1
    },
    .nmips = 1u,
    .format = NGF_IMAGE_FORMAT_RGB8,
    .usage_hint = NGF_IMAGE_USAGE_SAMPLE_FROM | NGF_IMAGE_USAGE_ATTACHMENT
  };
  ngf_image *offscreen_rt_texture = NULL;
  ngf_err = ngf_create_image(&offscreen_rt_texture_info, &offscreen_rt_texture);
 
  // Create a sampler for the textures.
  ngf_sampler_info sampler_info = {
    .min_filter = NGF_FILTER_LINEAR,
    .mag_filter = NGF_FILTER_LINEAR,
    .wrap_s = NGF_WRAP_MODE_CLAMP_TO_EDGE,
    .wrap_t = NGF_WRAP_MODE_CLAMP_TO_EDGE,
    .wrap_r = NGF_WRAP_MODE_CLAMP_TO_EDGE,
    .lod_max = 0,
    .lod_min = 0,
    .lod_bias = 0,
    .border_color = {0.0f}
  };
  ngf_sampler *texture_sampler = NULL;
  ngf_err = ngf_create_sampler(&sampler_info, &texture_sampler);
  assert(ngf_err == NGF_ERROR_OK);

  // Create a descriptor set layout.
  uint32_t *layout_data[] = {
    shader_modules[0].layout_metadata,
    shader_modules[1].layout_metadata
  };
  ngf_util_layout_data pipeline_layout_data;
  ngf_err = ngf_util_create_layout_data(layout_data, 2,
                                        &pipeline_layout_data);
  assert(ngf_err == NGF_ERROR_OK);
   
  // Create a descriptor set and write to it so that our texture and uniform
  // buffer are bound when the descriptor set is bound.
  ngf_descriptor_set *descriptor_set = NULL;
  ngf_create_descriptor_set(pipeline_layout_data.descriptors_layouts[0],
                            &descriptor_set);
  ngf_descriptor_write descriptor_writes[] = {
    {
      .binding = 0u,
      .type = NGF_DESCRIPTOR_UNIFORM_BUFFER,
      .op = {
        .buffer_bind = {
          .buffer = uniform_buffer,
          .offset = 0,
          .range = sizeof(uniform_data)
        }
      }
    },
    {
      .binding = 0u,
      .type = NGF_DESCRIPTOR_TEXTURE_AND_SAMPLER,
      .op = {
        .image_sampler_bind = {
          .image_subresource = {
            .image = texture,
            .mip_level = 0,
            .layer = 0,
            .layered = false
          },
          .sampler = texture_sampler
        }
      }
    },
    {
      .binding = 1u,
      .type = NGF_DESCRIPTOR_TEXTURE_AND_SAMPLER,
      .op = {
        .image_sampler_bind = {
          .image_subresource = {
            .image = offscreen_rt_texture,
            .mip_level = 0,
            .layer = 0,
            .layered = false
          },
          .sampler = texture_sampler
        }
      }
    }
  };
  ngf_err = ngf_apply_descriptor_writes(descriptor_writes, 3, descriptor_set);
  assert(ngf_err == NGF_ERROR_OK);

  // Create pipeline & draw operation.
  ngf_vertex_buf_bind_op vertex_buffer_binding = {
    .buffer = vertex_buffer,
    .offset = 0u,
    .binding = 0u
  };
  transient_state ts =
        create_transient_state(pipeline_layout_data.pipeline_layout,
                               descriptor_set,
                               800, 600,
                               shader_stages[0],
                               shader_stages[1],
                               &vertex_buffer_binding);

  // Create a rendertarget for off-screen rendering.
  ngf_attachment offscreen_rt_color_attachment = {
    .image_ref = {
      .image = offscreen_rt_texture,
      .mip_level = 0u,
      .layer = 0u,
      .layered = false
    },
    .type = NGF_ATTACHMENT_COLOR
  };
  ngf_render_target_info offscreen_rt_info = {
    .attachments = &offscreen_rt_color_attachment,
    .nattachments = 1u
  };
  ngf_render_target *offscreen_rt = NULL;
  ngf_err = ngf_create_render_target(&offscreen_rt_info, &offscreen_rt);

  // Create shader stages for the triangle.
  ngf_shader_stage *triangle_stages[] = {NULL, NULL};
  create_simple_shader_stages(&shader_modules[2].stage_info,
                              &shader_modules[3].stage_info,
                              triangle_stages);

  // Create a pipeline layout for the triangle's pipeline.
  ngf_util_layout_data tri_pipeline_layout_data;
  uint32_t *tri_layout_data[] = {
    shader_modules[2].layout_metadata,
    shader_modules[3].layout_metadata
  };
  ngf_err =
      ngf_util_create_layout_data(tri_layout_data, 2u,
                                  &tri_pipeline_layout_data);

  // Create a descriptor set for the triangle and write to it.
  ngf_descriptor_set *tri_descriptor_set = NULL;
  ngf_err =
      ngf_create_descriptor_set(tri_pipeline_layout_data.descriptors_layouts[0],
                                &tri_descriptor_set);
  assert(ngf_err == NGF_ERROR_OK);
  ngf_descriptor_write tri_descriptor_write = {
    .binding = 0u,
    .type = NGF_DESCRIPTOR_UNIFORM_BUFFER,
    .op = {
      .buffer_bind = {
        .buffer = uniform_buffer,
        .offset = 0,
        .range = sizeof(uniform_data)
      }
    }
  };
  ngf_err = ngf_apply_descriptor_writes(&tri_descriptor_write, 1,
                                        tri_descriptor_set);
  assert(ngf_err == NGF_ERROR_OK);

  // Create draw op and pipeline for offscreen rendering.
  transient_state offscreen_ts =
      create_transient_state(tri_pipeline_layout_data.pipeline_layout,
                             tri_descriptor_set,
                             512, 512,
                             triangle_stages[0],
                             triangle_stages[1],
                             NULL);

  // Obtain the default render target.
  ngf_render_target *rt;
  ngf_default_render_target(&rt);

  // Main loop.
  uniform_data ud;
  int old_width = 800, old_height = 600;
  while (!glfwWindowShouldClose(win)) {
    glfwPollEvents();
    int w, h;
    glfwGetWindowSize(win, &w, &h);
    if (w != old_width || h != old_height) {
      ngf_destroy_graphics_pipeline(ts.pipeline);
      ngf_destroy_draw_op(ts.draw_op);
      ngf_resize_context(ctx, w, h);
      ts = create_transient_state(pipeline_layout_data.pipeline_layout,
                                  descriptor_set,
                                  w, h,
                                  shader_stages[0],
                                  shader_stages[1],
                                  &vertex_buffer_binding);
      old_width = w; old_height = h;
    }
    ud.time = glfwGetTime();
    ngf_populate_buffer(uniform_buffer, 0, sizeof(uniform_data), &ud);
    ngf_begin_frame(ctx);
    ngf_execute_pass(pass, offscreen_rt, &(offscreen_ts.draw_op), 1);
    ngf_execute_pass(pass, rt, &(ts.draw_op), 1);
    ngf_end_frame(ctx);
  }
  return 0;
}

void create_simple_shader_stages(const ngf_shader_stage_info *vert,
                                 const ngf_shader_stage_info *frag,
                                 ngf_shader_stage **output) {
  ngf_shader_stage_info shader_stage_infos[] = {
    {
      .type = NGF_STAGE_VERTEX,
      .content = vert->content,
      .content_length = vert->content_length,
      .debug_name = "VERT stage"
    },
    {
      .type = NGF_STAGE_FRAGMENT,
      .content = frag->content,
      .content_length = frag->content_length,
      .debug_name = "FRAG stage" 
    },
  };
  ngf_error ngf_err =
    ngf_create_shader_stage(&shader_stage_infos[0], &output[0]);
  assert(ngf_err == NGF_ERROR_OK);
  ngf_err = ngf_create_shader_stage(&shader_stage_infos[1], &output[1]);
  assert(ngf_err == NGF_ERROR_OK);
}

transient_state create_transient_state(ngf_pipeline_layout *pipeline_layout,
                                       ngf_descriptor_set *descriptor_set,
                                       uint32_t width,
                                       uint32_t height,
                                       ngf_shader_stage *vert_stage,
                                       ngf_shader_stage *frag_stage,
                                       ngf_vertex_buf_bind_op *vert_binding)
{
  // Get a pipeline configuration prepopulated with OpenGL-style defaults.
  ngf_irect2d window_rect = {0, 0, width, height};
  ngf_util_graphics_pipeline_data pipeline_data;
  ngf_util_create_default_graphics_pipeline_data(NULL, &window_rect,
                                                 &pipeline_data);
  
  // Set shader stages.
  pipeline_data.pipeline_info.shader_stages[0] = vert_stage;
  pipeline_data.pipeline_info.shader_stages[1] = frag_stage;
  pipeline_data.pipeline_info.nshader_stages = 2;

  // Set pipeline layout.
  pipeline_data.pipeline_info.layout = pipeline_layout;

  // Configure vertex attribute format.
  ngf_vertex_attrib_desc color_attrib_info = {
    .binding = 0,
    .location = 0,
    .size = 2,
    .normalized = false,
    .type = NGF_TYPE_FLOAT
  };
  ngf_vertex_buf_binding_desc color_attrib_binding_info = {
    .input_rate = NGF_INPUT_RATE_VERTEX,
    .stride = sizeof(float) * 2
  };
  pipeline_data.vertex_input_info.nattribs = vert_binding != NULL ? 1 : 0;
  pipeline_data.vertex_input_info.attribs = &color_attrib_info;
  pipeline_data.vertex_input_info.nvert_buf_bindings =
    vert_binding != NULL ? 1 : 0;
  pipeline_data.vertex_input_info.vert_buf_bindings =
    &color_attrib_binding_info;

  // Create a pipeline object with the given configuration.
  ngf_graphics_pipeline *pipeline = NULL;
  ngf_error ngf_err =
    ngf_create_graphics_pipeline(&pipeline_data.pipeline_info, &pipeline);
  assert(ngf_err == NGF_ERROR_OK);

  // Create draw operation.
  ngf_descriptor_set_bind_op descriptor_set_binding = {
    .set = descriptor_set,
    .slot = 0u
  };
  ngf_draw_subop_info subop_info = {
    .ndynamic_state_cmds = 0,
    .first_element = 0,
    .nelements = vert_binding != NULL ? 6 : 3,
    .mode = NGF_DRAW_MODE_DIRECT,
    .nvertex_buf_bind_ops = vert_binding != NULL ? 1 : 0,
    .vertex_buf_bind_ops = vert_binding,
    .ndescriptor_set_bind_ops = 1,
    .descriptor_set_bind_ops = &descriptor_set_binding,
    .index_buf_bind_op = NULL
  };
  ngf_draw_op_info op_info = {
    .nsubops = 1,
    .subops = &subop_info,
    .pipeline = pipeline
  };
  ngf_draw_op *op = NULL;
  ngf_err = ngf_create_draw_op(&op_info, &op);
  assert(ngf_err == NGF_ERROR_OK);
  
  transient_state result = {
    .pipeline = pipeline,
    .draw_op = op
  };

  return result;
}

void load_shader_module(const char *shader_name,
                        const char *shader_layout_name,
                        shader_module *sm) {
  FILE *shader_content_file = fopen(shader_name, "r");
  uint32_t shader_content_length = filelen(shader_content_file);
  char *shader_content = malloc(shader_content_length);
  sm->stage_info.content_length = shader_content_length;
  fread(shader_content, sizeof(char), sm->stage_info.content_length,
        shader_content_file);
  sm->stage_info.content = shader_content;
  fclose(shader_content_file);
  FILE *shader_layout_file = fopen(shader_layout_name, "r");
  uint32_t shader_layout_length = filelen(shader_layout_file);
  sm->layout_metadata = malloc(shader_layout_length);
  fread(sm->layout_metadata, sizeof(uint32_t),
        shader_layout_length << 2, shader_layout_file);
  fclose(shader_layout_file);
}

