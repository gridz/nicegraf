#include "common.h"

#include "nicegraf.h"
#include "nicegraf_wrappers.h"

#include <assert.h>
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>
#include <stdio.h>

void main_loop(GLFWwindow *win, ngf_context *c);
void on_window_resize(GLFWwindow *win, int w, int h);

void resize_swapchain(GLFWwindow *win, int w, int h) {
  ngf_context *c =
    (ngf_context*)glfwGetWindowUserPointer(win);
  ngf_error err = ngf_resize_context(c, w, h);
  assert(err == NGF_ERROR_OK);
  on_window_resize(win, w, h);
}

void ngf_common_start_app(const char *title, int argc, char *argv[]) {
  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
  GLFWwindow *win = glfwCreateWindow(800, 600, title, nullptr, nullptr);
  assert(win);
  glfwSetWindowSizeCallback(win, resize_swapchain);
  ngf_error err = ngf_initialize(NGF_DEVICE_PREFERENCE_DONTCARE);
  assert(err == NGF_ERROR_OK);
  ngf_swapchain_info sci;
  sci.cfmt = NGF_IMAGE_FORMAT_BGRA8;
  sci.dfmt = NGF_IMAGE_FORMAT_UNDEFINED;
  sci.nsamples = 16;
  sci.capacity_hint = 2;
  sci.width = 800;
  sci.height = 600;
  sci.present_mode = NGF_PRESENTATION_MODE_FIFO;
  sci.native_handle = (uintptr_t)NGF_GLFW_GET_HANDLE(win);
  ngf_context_info ci;
  ci.swapchain_info = &sci,
  ci.shared_context = NULL;
  ngf_owned_context ctx;
  err = ctx.initialize(&ci);
  assert(err == NGF_ERROR_OK);
  main_loop(win, ctx.get());
  glfwTerminate();
}
