#version 430
precision mediump float;
out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};
layout(location=0) in vec2 i_TexCoord;
layout(std140, binding=0) uniform UniformData {
 float u_Time;
};
out vec2 v_TexCoords;
void main() {
  const vec2 positions[] = vec2[](
    vec2( 0.25,  0.25),
    vec2(-0.25,  0.25),
    vec2(-0.25, -0.25),
    vec2( 0.25,  0.25),
    vec2(-0.25, -0.25),
    vec2( 0.25, -0.25));
  vec2 pos = positions[gl_VertexIndex % 6];
  float scale = 2.0 * (sin(u_Time) + 1.0);
  v_TexCoords = i_TexCoord;
  gl_Position =
    vec4(scale * pos * vec2(cos(u_Time), sin(u_Time)), 0.0, 1.0);
}

