#version 430
precision mediump float;
out vec3 o_Color;
out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};
layout(std140, binding=0) uniform UniformData {
 float u_Time;
};
void main() {
 const vec3 data[] = vec3[](
   vec3( 0.0,  1.0,  0.0),
   vec3(-1.0, -1.0,  0.0),
   vec3( 1.0, -1.0,  0.0));
 vec3 p = data[gl_VertexIndex % 3];
 o_Color = 0.5 * p + vec3(0.5);
 float s = sin(u_Time), c = cos(u_Time);
 gl_Position = vec4(p.x * c - p.y * s, p.x * s + p.y * c, 0.0, 1.0);
}
