#version 430
precision mediump float;
layout(binding=0) uniform sampler2D u_Texture;
layout(binding=1) uniform sampler2D u_OtherTexture;
in vec2 v_TexCoords;
out vec4 o_FragColor;
void main() {
  o_FragColor = 0.5 * texture(u_Texture, v_TexCoords) +
                0.5 * texture(u_OtherTexture, v_TexCoords);
}
