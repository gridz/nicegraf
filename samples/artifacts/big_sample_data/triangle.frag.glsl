#version 430
precision mediump float;
in vec3 o_Color;
out vec4 o_FragColor;
void main() { o_FragColor = vec4(o_Color, 1.0); }
