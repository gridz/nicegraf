#version 430

layout(binding = 0) uniform sampler2D u_Texture;
layout(binding = 1) uniform sampler2D u_OtherTexture;

layout(location = 0) out vec4 o_FragColor;
layout(location = 0) in vec2 v_TexCoords;

void main()
{
    o_FragColor = (texture(u_Texture, v_TexCoords) * 0.5) + (texture(u_OtherTexture, v_TexCoords) * 0.5);
}

