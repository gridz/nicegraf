#version 430

out gl_PerVertex
{
    vec4 gl_Position;
    float gl_PointSize;
    float gl_ClipDistance[1];
};

const vec2 _19[6] = vec2[](vec2(0.25), vec2(-0.25, 0.25), vec2(-0.25), vec2(0.25), vec2(-0.25), vec2(0.25, -0.25));

layout(binding = 0, std140) uniform UniformData
{
    float u_Time;
} _35;

layout(location = 0) out vec2 v_TexCoords;
layout(location = 0) in vec2 i_TexCoord;

void main()
{
    vec2 pos = _19[gl_VertexID % 6];
    float scale = 2.0 * (sin(_35.u_Time) + 1.0);
    v_TexCoords = i_TexCoord;
    gl_Position = vec4((pos * scale) * vec2(cos(_35.u_Time), sin(_35.u_Time)), 0.0, 1.0);
}

