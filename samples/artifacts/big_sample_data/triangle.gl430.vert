#version 430

out gl_PerVertex
{
    vec4 gl_Position;
    float gl_PointSize;
    float gl_ClipDistance[1];
};

const vec3 _19[3] = vec3[](vec3(0.0, 1.0, 0.0), vec3(-1.0, -1.0, 0.0), vec3(1.0, -1.0, 0.0));

layout(binding = 0, std140) uniform UniformData
{
    float u_Time;
} _41;

layout(location = 0) out vec3 o_Color;

void main()
{
    vec3 p = _19[gl_VertexID % 3];
    o_Color = (p * 0.5) + vec3(0.5);
    float s = sin(_41.u_Time);
    float c = cos(_41.u_Time);
    gl_Position = vec4((p.x * c) - (p.y * s), (p.x * s) + (p.y * c), 0.0, 1.0);
}

