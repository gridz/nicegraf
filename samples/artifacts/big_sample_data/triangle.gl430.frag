#version 430

layout(location = 0) out vec4 o_FragColor;
layout(location = 0) in vec3 o_Color;

void main()
{
    o_FragColor = vec4(o_Color, 1.0);
}

