#version 430
layout(std140, binding = 0) uniform Params {
  vec2 u_ScreenSize;
  float u_Time;
};

out vec4 o_FragColor;

void main() {
  vec2 uv = gl_FragCoord.xy / u_ScreenSize;
  vec2 xy = uv * 2.0 - 1.0;
  float dist = dot(xy, xy);
  float err = (1.0 - dist);
  vec4 color = vec4(uv, (1.0 + sin(u_Time)) * 0.5, 1.0);
  o_FragColor = sin(err) * color;
}

