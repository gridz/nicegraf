#include "common.h"
#include "nicegraf_util.h"
#include "stb_image.h"
#include <GLFW/glfw3.h>
#include <nicegraf.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <thread>

ngf_graphics_pipeline *pipeline = nullptr;
ngf_graphics_program *program = nullptr;
ngf_draw_op *draw_op = nullptr;
ngf_buffer *vertex_buffer = nullptr;
ngf_pipeline_layout *pipeline_layout = nullptr;
ngf_descriptors_layout *descriptors_layout = nullptr;
ngf_descriptor_set *res_set = nullptr;

void recreate_pipelines(size_t win_size_x, size_t win_size_y) {
  if (pipeline) {
    ngf_destroy_graphics_pipeline(pipeline);
  }
  ngf_irect2d window_rect = {0, 0, win_size_x, win_size_y};
  ngf_util_graphics_pipeline_data pipeline_data;
  ngf_util_create_default_graphics_pipeline_data(
    program,
    nullptr,
    &window_rect,
    &pipeline_data);
  ngf_vertex_attrib_info ais[1];
  ais[0].binding = 0;
  ais[0].location = 0;
  ais[0].offset = 0;
  ais[0].size = 3;
  ais[0].type = NGF_TYPE_FLOAT;
  ngf_vertex_binding_info bi;
  bi.input_rate = NGF_INPUT_RATE_VERTEX;
  bi.stride = sizeof(float) * 3;
  pipeline_data.vertex_input_info.nattribs = 0;
  pipeline_data.vertex_input_info.attribs = ais;
  pipeline_data.vertex_input_info.nbindings = 0;
  pipeline_data.vertex_input_info.bindings = &bi;
  pipeline_data.pipeline_info.layout = pipeline_layout;
  ngf_create_graphics_pipeline(&pipeline_data.pipeline_info, &pipeline);

  ngf_draw_op_info doi;
  ngf_draw_subop_info si;
  si.ndynamic_state_cmds = 0;
  si.first_element = 0;
  si.nelements = 6;
  si.mode = NGF_DRAW_MODE_DIRECT;
  si.primitive = NGF_PRIMITIVE_TYPE_TRIANGLE_LIST;
  si.nvertex_buffer_bindings = 0;
  si.vertex_buffer_bindings = nullptr;
  si.ndescriptor_set_bindings = 1;
  ngf_descriptor_set_binding rsb{
    res_set,
    0
  };
  si.descriptor_set_bindings = &rsb;
  si.index_buffer_binding = nullptr;
  doi.nsubops = 1;
  doi.subops = &si;
  doi.pipeline = pipeline;
  ngf_create_draw_op(&doi,&draw_op);
}

const char *QUAD_VERT_SHADER = R"(#version 430
precision mediump float;
out vec2 v_UV;
void main() {
  const vec2 positions[] = vec2[](
    vec2( 1.0,  1.0),
    vec2(-1.0,  1.0),
    vec2(-1.0, -1.0),
    vec2( 1.0,  1.0),
    vec2(-1.0, -1.0),
    vec2( 1.0, -1.0));
  gl_Position = vec4(positions[gl_VertexID % 6], 0.0, 1.0);
  v_UV = 0.5 * (gl_Position.xy + vec2(1.0));
}
)";

const char *QUAD_FRAG_SHADER = R"(#version 430
precision mediump float;
layout(binding = 8) uniform sampler2D u_Texture;
in vec2 v_UV;
out vec4 o_FragColor;
void main() {
  o_FragColor = texture(u_Texture, v_UV);
}
)";


void main_loop(GLFWwindow *win, ngf_common_context_and_swapchain c) {
  int w, h;
  glfwGetWindowSize(win, &w, &h);
  ngf_error err = ngf_set_context(c.ctx, c.sc);
  ngf_graphics_program_info gpi;
  gpi.nstages = 2;
  gpi.stages[0].type = NGF_STAGE_VERTEX;
  gpi.stages[0].content = QUAD_VERT_SHADER;
  gpi.stages[0].content_length = strlen(QUAD_VERT_SHADER);
  gpi.stages[1].type = NGF_STAGE_FRAGMENT;
  gpi.stages[1].content = QUAD_FRAG_SHADER;
  gpi.stages[1].content_length = strlen(QUAD_FRAG_SHADER);
  err = ngf_build_graphics_programs(&gpi, 1, &program);
  assert(err == NGF_ERROR_OK);
  ngf_pass_info pi;
  ngf_clear_info ci;
  ngf_attachment_load_op lop = NGF_LOAD_OP_CLEAR;
  memset(ci.clear_color, 0, sizeof(ci.clear_color));
  pi.clears = &ci;
  pi.loadops = &lop;
  pi.nloadops = 1;

  ngf_descriptors_layout_info rli;
  ngf_descriptor_info rbi;
  ngf_pipeline_layout_info pli;
  rbi.id = 8;
  rbi.type = NGF_DESCRIPTOR_TEXTURE_AND_SAMPLER;
  rli.ndescriptors = 1;
  rli.descriptors = &rbi;
  ngf_create_descriptors_layout(&rli, &descriptors_layout);
  ngf_create_descriptor_set(descriptors_layout, &res_set);
  pli.ndescriptors_layouts = 1;
  pli.descriptors_layouts = &descriptors_layout;
  ngf_create_pipeline_layout(&pli, &pipeline_layout);
  ngf_sampler *sampler;
  ngf_sampler_info si;
  si.lod_bias = 0;
  si.lod_max = 0;
  si.lod_min = 0;
  si.mag_filter = NGF_FILTER_LINEAR;
  si.min_filter = NGF_FILTER_LINEAR;
  si.wrap_r = NGF_WRAP_MODE_CLAMP_TO_EDGE;
  si.wrap_s = NGF_WRAP_MODE_CLAMP_TO_EDGE;
  si.wrap_t = NGF_WRAP_MODE_CLAMP_TO_EDGE;
  ngf_create_sampler(&si, &sampler);
  ngf_descriptor_write resbind;
  resbind.index = 0;
  resbind.type = NGF_DESCRIPTOR_TEXTURE_AND_SAMPLER;
  resbind.op.image_sampler_bind.sampler = sampler;
  int iw, ih;
  uint8_t * image_data =
      stbi_load("lenna.png", &iw, &ih, nullptr, 0);
  ngf_image *texture_img;
  ngf_image_info ii;
  ii.nmips = 1;
  ii.nsamples = 1;
  ii.type = NGF_IMAGE_TYPE_IMAGE_2D;
  ii.usage_hint = NGF_IMAGE_USAGE_SAMPLE_FROM;
  ii.format = NGF_IMAGE_FORMAT_RGB8;
  ii.extent.depth = 1;
  ii.extent.width = iw; ii.extent.height = ih;
  ngf_create_image(&ii, &texture_img);
  assert(image_data);
  ngf_populate_image(texture_img, 0, {0, 0, 0}, ii.extent, image_data);
  resbind.op.image_sampler_bind.image_subresource.image = texture_img;
  resbind.op.image_sampler_bind.image_subresource.layer = 0;
  resbind.op.image_sampler_bind.image_subresource.layered = false;
  resbind.op.image_sampler_bind.image_subresource.mip_level = 0;
  ngf_apply_descriptor_writes(&resbind, 1, res_set);
  ngf_pass *pass;
  ngf_create_pass(&pi, &pass);
  ngf_render_target *rt;
  ngf_default_render_target(&rt);

  recreate_pipelines(w, h);
  while (!glfwWindowShouldClose(win)) {
    glfwPollEvents();
    ngf_execute_pass(pass, rt, draw_op, 1);
    ngf_present(c.sc);
  }
}

void on_window_resize(GLFWwindow *win, int w, int h){
  recreate_pipelines(w, h);
}

int main(int argc, char *argv[]) { 
  ngf_common_start_app("Textured quad", argc, argv);
  return 0;
}
