#include "common.h"
#include "nicegraf.h"
#include "nicegraf_util.h"
#include "nicegraf_wrappers.h"
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

ngf_owned_graphics_pipeline pipeline; 
ngf_owned_draw_op draw_op;
ngf_owned_buffer vertex_buffer;
ngf_owned_shader_stage stages[2];

void recreate_pipelines(size_t win_size_x, size_t win_size_y) {
  pipeline.reset(nullptr);
  draw_op.reset(nullptr);
  ngf_irect2d window_rect = {0, 0, win_size_x, win_size_y};
  ngf_util_graphics_pipeline_data pipeline_data;
  ngf_util_create_default_graphics_pipeline_data(
    nullptr,
    &window_rect,
    &pipeline_data);
  pipeline_data.pipeline_info.shader_stages[0] = stages[0].get();
  pipeline_data.pipeline_info.shader_stages[1] = stages[1].get();
  pipeline_data.pipeline_info.nshader_stages = 2;
  ngf_vertex_attrib_desc ais[1];
  ais[0].binding = 0;
  ais[0].location = 0;
  ais[0].offset = 0;
  ais[0].size = 3;
  ais[0].normalized = false;
  ais[0].type = NGF_TYPE_FLOAT;
  ngf_vertex_buf_binding_desc bi;
  bi.binding = 0u;
  bi.input_rate = NGF_INPUT_RATE_VERTEX;
  bi.stride = sizeof(float) * 3;
  pipeline_data.vertex_input_info.nattribs = 1;
  pipeline_data.vertex_input_info.attribs = ais;
  pipeline_data.vertex_input_info.nvert_buf_bindings = 1;
  pipeline_data.vertex_input_info.vert_buf_bindings = &bi;
  pipeline.initialize(&pipeline_data.pipeline_info);

  ngf_draw_op_info doi;
  ngf_draw_subop_info si;
  si.ndynamic_state_cmds = 0;
  si.first_element = 0;
  si.nelements = 3;
  si.mode = NGF_DRAW_MODE_DIRECT;
  si.nvertex_buf_bind_ops = 1;
  si.ndescriptor_set_bind_ops = 0;
  ngf_vertex_buf_bind_op vbi;
  vbi.buffer = vertex_buffer.get();
  vbi.binding = 0;
  vbi.offset = 0;
  si.vertex_buf_bind_ops = &vbi;
  si.descriptor_set_bind_ops = nullptr;
  si.index_buf_bind_op = nullptr;
  doi.nsubops = 1;
  doi.subops = &si;
  doi.pipeline = pipeline.get();
  draw_op.initialize(&doi);
}

const char *TRIANGLE_VERT_SHADER = R"(#version 430
precision mediump float;
layout(location=0) in vec3 i_Color;
out vec3 v_Color;
void main() {
  const vec2 positions[] = vec2[](
    vec2( 0.0,  1.0),
    vec2(-1.0, -1.0),
    vec2( 1.0, -1.0));
  v_Color = i_Color;
  gl_Position = vec4(positions[gl_VertexID % 3], 0.0, 1.0);
}
)";

const char *TRIANGLE_FRAG_SHADER = R"(#version 430
precision mediump float;
in vec3 v_Color;
out vec4 o_FragColor;
void main() {
  o_FragColor = vec4(v_Color, 1.0);
}
)";
void main_loop(GLFWwindow *win, ngf_context *c) {
  int w, h;
  glfwGetWindowSize(win, &w, &h);
  ngf_error err = ngf_set_context(c);
  assert(err == NGF_ERROR_OK);
  ngf_shader_stage_info stages_info[2];
  stages_info[0].type = NGF_STAGE_VERTEX;
  stages_info[0].content = TRIANGLE_VERT_SHADER;
  stages_info[0].content_length = strlen(TRIANGLE_VERT_SHADER);
  stages_info[1].type = NGF_STAGE_FRAGMENT;
  stages_info[1].content = TRIANGLE_FRAG_SHADER;
  stages_info[1].content_length = strlen(TRIANGLE_FRAG_SHADER);
  err = stages[0].initialize(&stages_info[0]);
  assert(err == NGF_ERROR_OK);
  err = stages[1].initialize(&stages_info[1]);
  assert(err == NGF_ERROR_OK);
  ngf_pass_info pi;
  ngf_clear_info ci;
  ngf_attachment_load_op lop = NGF_LOAD_OP_CLEAR;
  memset(ci.clear_color, 0, sizeof(ci.clear_color));
  pi.clears = &ci;
  pi.loadops = &lop;
  pi.nloadops = 1;
  ngf_buffer_info bufi;
  bufi.access_freq = NGF_BUFFER_USAGE_STATIC;
  bufi.access_type = NGF_BUFFER_ACCESS_DRAW;
  bufi.size = 3 * sizeof(float) * 3;
  bufi.type = NGF_BUFFER_TYPE_VERTEX;
  vertex_buffer.initialize(&bufi);
  float buffer_data[]{
    1.0, 0.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 0.0, 1.0
  };
  ngf_populate_buffer(vertex_buffer.get(), 0, sizeof(buffer_data), buffer_data);
  ngf_owned_pass pass;
  pass.initialize(&pi);
  ngf_render_target *rt;
  ngf_default_render_target(&rt);
  recreate_pipelines(w, h);
  while (!glfwWindowShouldClose(win)) {
    glfwPollEvents();
    ngf_begin_frame(c);
    ngf_draw_op *op = draw_op.get();
    ngf_execute_pass(pass.get(), rt, &op, 1);
    ngf_end_frame(c);
  }
}

void on_window_resize(GLFWwindow *win, int w, int h) {
  glfwGetWindowSize(win, &w, &h);
  recreate_pipelines(w, h);
}

int main(int argc, char *argv[]) {
  ngf_common_start_app("Triangle", argc, argv);
  return 0;
}
