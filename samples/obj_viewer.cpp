#include <nicegraf.h>
#include <nicegraf_util.h>
#include <nicegraf_wrappers.h>
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>
#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <vector>
#include <string>
#include <unordered_map>

const char *VERTEX_SHADER_SOURCE = R"(#version 430
layout(location = 0) in vec3 i_Position;
layout(location = 1) in vec3 i_Normal;
layout(location = 2) in vec2 i_TexCoord;

layout(std140, binding = 0) uniform UniformBlock {
  mat4 u_ViewProjection;
};

void main() {
  gl_Position = vec4(i_Position, 1.0) * u_ViewProjection;
}
)";

const char *FRAGMENT_SHADER_SOURCE = R"(#version 430
out vec4 o_FragColor;
void main() {
  o_FragColor = vec4(1.0);
}
)";

// Attributes for a single vertex.
struct vertex {
  float position[3];
  float normal[3];
  float texcoord[2];
};

struct uniform_buffer {
  glm::mat4 view_projection;
};

// Holds the entire state of the OBJ viewer application.
struct obj_viewer_state {
  ngf_owned_context context;
  ngf_owned_shader_stage shader_stages[2];
  std::vector<ngf_owned_buffer> vertex_attrib_buffers;
  std::vector<ngf_owned_buffer> index_buffers;
  ngf_owned_buffer uniform_buffer;
  ngf_owned_pipeline_layout pipeline_layout;
  ngf_owned_descriptors_layout descriptors_layout;
  ngf_owned_graphics_pipeline pipeline;
  ngf_owned_draw_op draw_op;
  ngf_owned_descriptor_set uniform_buffer_ds;
  ngf_owned_pass renderpass;
};

void debug_callback(const char *msg, const void *userdata) {
  fprintf(stderr, "%s\n", msg);
}

int main(int argc, char *argv[]) {
  obj_viewer_state app_state;
  const char *obj_file_name = NULL;

  // Get the file name to load.
  if (argc <= 1) {
    printf("Usage: obj_viewer [OBJ file]\n");
    return 0;
  } else {
    obj_file_name = argv[1];
  }

  // Create a GLFW window.
  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); // Make sure GLFW doesn't try
                                                // to create a context for us.
  GLFWwindow *win = glfwCreateWindow(800, 600, "OBJ viewer", NULL, NULL);
  assert(win);
  
  // Initialize Nicegraf.
  ngf_error ngf_err = ngf_initialize(NGF_DEVICE_PREFERENCE_DONTCARE);
  assert(ngf_err == NGF_ERROR_OK);

  // Create a Nicegraf context.
  ngf_swapchain_info swapchain_info = {
    .cfmt = NGF_IMAGE_FORMAT_BGRA8,
    .dfmt = NGF_IMAGE_FORMAT_UNDEFINED,
    .nsamples = 16, // 16x MSAA
    .capacity_hint = 2, // Double-buffered
    .width = 800,
    .height = 600,
    .native_handle = NGF_GLFW_GET_HANDLE(win)
  };
  ngf_context_info ctx_info = {
    .swapchain_info = &swapchain_info,
    .shared_context = NULL
  };
  ngf_err = app_state.context.initialize(&ctx_info);
  assert(ngf_err == NGF_ERROR_OK);

  // Activate context.
  ngf_set_context(app_state.context.get());
  ngf_debug_message_callback(NULL, debug_callback);

  // Load scene data.
  tinyobj::attrib_t attribs;
  std::vector<tinyobj::shape_t> shapes;
  std::vector<tinyobj::material_t> materials;
  std::string obj_err;
  bool ok = tinyobj::LoadObj(&attribs,
                             &shapes,
                             &materials,
                             &obj_err,
                              obj_file_name);
  if (!ok) {
    printf("Error while loading %s: %s\n", obj_file_name, obj_err.c_str());
    exit(1);
  }

  std::vector<ngf_draw_subop_info> shape_subop_infos;
  for (const tinyobj::shape_t &shape : shapes) {
    std::vector<uint32_t> index_data;
    std::vector<vertex> vertex_attrib_data;
    std::unordered_map<std::string, uint32_t> idx_map;
    uint32_t last_attr_idx = 0u;
    for (const tinyobj::index_t &idx : shape.mesh.indices) {
      std::string idx_key = std::to_string(idx.vertex_index) + " " +
                            std::to_string(idx.normal_index) + " " +
                            std::to_string(idx.texcoord_index);
      uint32_t attr_idx = 0u;
      auto idx_it = idx_map.find(idx_key);
      if (idx_it != idx_map.end()) {
        attr_idx = idx_it->second;
      } else {
        attr_idx = last_attr_idx++;
        idx_map[idx_key] = attr_idx;
        tinyobj::real_t vx = attribs.vertices[idx.vertex_index * 3 + 0];
        tinyobj::real_t vy = attribs.vertices[idx.vertex_index * 3 + 1];
        tinyobj::real_t vz = attribs.vertices[idx.vertex_index * 3 + 2];
        tinyobj::real_t nx = attribs.normals[idx.normal_index * 3 + 0];
        tinyobj::real_t ny = attribs.normals[idx.normal_index * 3 + 1];
        tinyobj::real_t nz = attribs.normals[idx.normal_index * 3 + 2];
        tinyobj::real_t tu = 0.0f;
        tinyobj::real_t tv = 0.0f;
        if (attribs.texcoords.size() != 0) {
          tu = attribs.texcoords[idx.texcoord_index * 2 + 0];
          tv = attribs.texcoords[idx.texcoord_index * 2 + 1];
        }
        vertex_attrib_data.push_back({{vx, vy, vz}, {nx, ny, nz}, {tu, tv}});
      }
      index_data.push_back(attr_idx);
    }
    ngf_buffer_info vertex_attrib_buffer_info = {
      .size = sizeof(vertex) * vertex_attrib_data.size(),
      .type = NGF_BUFFER_TYPE_VERTEX,
      .access_freq = NGF_BUFFER_USAGE_STATIC,
      .access_type = NGF_BUFFER_ACCESS_DRAW
    };
    ngf_owned_buffer vertex_attrib_buffer;
    ngf_err = vertex_attrib_buffer.initialize(&vertex_attrib_buffer_info);
    assert(ngf_err == NGF_ERROR_OK);
    ngf_populate_buffer(vertex_attrib_buffer.get(), 0,
                        vertex_attrib_buffer_info.size,
                        (void*)vertex_attrib_data.data());
    ngf_buffer_info index_buffer_info = {
      .size = sizeof(uint32_t) * index_data.size(),
      .type = NGF_BUFFER_TYPE_INDEX,
      .access_freq = NGF_BUFFER_USAGE_STATIC,
      .access_type = NGF_BUFFER_ACCESS_DRAW
    };
    ngf_owned_buffer index_buffer;
    ngf_err = index_buffer.initialize(&index_buffer_info);
    assert(ngf_err == NGF_ERROR_OK);
    ngf_populate_buffer(index_buffer.get(), 0,
                        index_buffer_info.size,
                        (void*)index_data.data());
    app_state.vertex_attrib_buffers.emplace_back(
        std::move(vertex_attrib_buffer));
    app_state.index_buffers.emplace_back(std::move(index_buffer));
    ngf_vertex_buf_bind_op vertex_buf_bind_op = {
      .buffer = app_state.vertex_attrib_buffers.back().get(),
      .offset = 0u,
      .binding = 0u
    };
    ngf_index_buf_bind_op index_buf_bind_op = {
      .buffer = app_state.index_buffers.back().get(),
      .type = NGF_TYPE_UINT32
    };
    ngf_draw_subop_info shape_subop = {
      .dynamic_state_cmds = nullptr,
      .ndynamic_state_cmds = 0u,
      .ndescriptor_set_bind_ops = 0u,
      .descriptor_set_bind_ops = nullptr,
      .vertex_buf_bind_ops = &vertex_buf_bind_op,
      .nvertex_buf_bind_ops = 1u,
      .mode = NGF_DRAW_MODE_INDEXED,
      .index_buf_bind_op = &index_buf_bind_op,
      .primitive = NGF_PRIMITIVE_TYPE_TRIANGLE_LIST,
      .first_element = 0u,
      .nelements = (uint32_t)index_data.size(),
      .ninstances = 1u
    };
    shape_subop_infos.push_back(shape_subop);
  }
  printf("SHAPE SUBOP INFOS %d\n", shape_subop_infos.size());

  // Create shader stages.
  ngf_shader_stage_info shader_stage_infos[] = {
    {
      .type = NGF_STAGE_VERTEX,
      .content = VERTEX_SHADER_SOURCE,
      .content_length = strlen(VERTEX_SHADER_SOURCE),
      .debug_name = "Vert Stage"
    },
    {
      .type = NGF_STAGE_FRAGMENT,
      .content = FRAGMENT_SHADER_SOURCE,
      .content_length = strlen(FRAGMENT_SHADER_SOURCE),
      .debug_name = "Frag Stage"
    }
  };
  ngf_err = app_state.shader_stages[0].initialize(&shader_stage_infos[0]);
  assert(ngf_err == NGF_ERROR_OK);
  ngf_err = app_state.shader_stages[1].initialize(&shader_stage_infos[0]);
  assert(ngf_err == NGF_ERROR_OK);

  // Create pipeline layout.
  ngf_descriptor_info descriptor_info = {
    .type = NGF_DESCRIPTOR_UNIFORM_BUFFER,
    .id = 0u
  };
  ngf_util_layout_data spld;
  ngf_err = ngf_util_create_simple_layout_data(&descriptor_info, 1u, &spld);
  assert(ngf_err == NGF_ERROR_OK);
  app_state.pipeline_layout.reset(spld.pipeline_layout);
  app_state.descriptors_layout.reset(spld.descriptors_layouts[0]);

  // Configure and create the pipeline.
  ngf_util_graphics_pipeline_data pipeline_data;
  ngf_irect2d viewport = {
    .x = 0,
    .y = 0,
    .width = 800,
    .height = 600
  };
  ngf_util_create_default_graphics_pipeline_data(
      app_state.pipeline_layout.get(), &viewport, &pipeline_data);
  pipeline_data.pipeline_info.shader_stages[0] =
      app_state.shader_stages[0].get();
  pipeline_data.pipeline_info.shader_stages[1] =
      app_state.shader_stages[1].get();
  pipeline_data.pipeline_info.nshader_stages = 2;
  pipeline_data.depth_stencil_info.depth_test = true;
  pipeline_data.depth_stencil_info.depth_write = true;
  pipeline_data.depth_stencil_info.depth_compare = NGF_LESS;
  ngf_vertex_buf_binding_desc attrib_buf_binding_desc = {
    .binding = 0u,
    .stride = sizeof(float) * (3 + 3 + 2),
    .input_rate = NGF_INPUT_RATE_VERTEX
  };
  ngf_vertex_attrib_desc attrib_descs[] = {
    {
      .location = 0u,
      .binding = 0u,
      .offset = 0u,
      .type = NGF_TYPE_FLOAT,
      .size = 3u,
      .normalized = false
    },
    {
      .location = 1u,
      .binding = 0u,
      .offset = sizeof(float) * 3,
      .type = NGF_TYPE_FLOAT,
      .size = 3u,
      .normalized = false
    },
    {
      .location = 2u,
      .binding = 0u,
      .offset = sizeof(float) * (3 + 2),
      .type = NGF_TYPE_FLOAT,
      .size = 2u,
      .normalized = false
    }
  };
  ngf_vertex_input_info input_info = {
    .vert_buf_bindings = &attrib_buf_binding_desc,
    .nvert_buf_bindings = 1u,
    .attribs = attrib_descs,
    .nattribs = 3u
  };
  pipeline_data.vertex_input_info = input_info;
  pipeline_data.multisample_info.multisample = true;
  ngf_err = app_state.pipeline.initialize(&pipeline_data.pipeline_info);
  assert(ngf_err == NGF_ERROR_OK);

  // Create the draw operation.
  ngf_draw_op_info draw_op_info = {
    .pipeline = app_state.pipeline.get(),
    .subops = shape_subop_infos.data(),
    .nsubops = (uint32_t)shape_subop_infos.size()
  };
  ngf_err = app_state.draw_op.initialize(&draw_op_info);
  assert(ngf_err == NGF_ERROR_OK);

  // Create the uniform buffer.
  ngf_buffer_info uniform_buffer_info = {
    .size = sizeof(uniform_buffer),
    .type = NGF_BUFFER_TYPE_UNIFORM,
    .access_freq = NGF_BUFFER_USAGE_DYNAMIC,
    .access_type = NGF_BUFFER_ACCESS_DRAW
  };
  ngf_err = app_state.uniform_buffer.initialize(&uniform_buffer_info);
  assert(ngf_err == NGF_ERROR_OK);

  // Create a descriptor set and write to it.
  ngf_err = app_state.uniform_buffer_ds.initialize(
      app_state.descriptors_layout.get());
  assert(ngf_err == NGF_ERROR_OK);
  ngf_descriptor_write ds_write = {
    .binding = 0u,
    .type = NGF_DESCRIPTOR_UNIFORM_BUFFER,
    .op = {
      .buffer_bind = {
        .buffer = app_state.uniform_buffer.get(),
        .offset = 0u,
        .range = sizeof(uniform_buffer)
      }
    }
  };
  ngf_err = ngf_apply_descriptor_writes(&ds_write, 1,
                                         app_state.uniform_buffer_ds.get());

  // Create renderpass.
  ngf_clear_info clears[] = {
    {
      .clear_color = {0.0f, 0.0f, 0.0f, 0.0f},
      .clear_depth = 1.0f,
      .clear_stencil = 0u
    },
    {
      .clear_color = {0.0f, 0.0f, 0.0f, 0.0f},
      .clear_depth = 1.0f,
      .clear_stencil = 0u
    }
  };
  ngf_attachment_load_op load_ops[] = {
    NGF_LOAD_OP_CLEAR,
    NGF_LOAD_OP_CLEAR
  };
  ngf_pass_info pass_info = {
    .clears = clears,
    .loadops = load_ops,
    .nloadops = 2
  };
  ngf_err = app_state.renderpass.initialize(&pass_info);
  assert(ngf_err == NGF_ERROR_OK);
  
  // Create and execute a draw op to bind our matrix descriptor set.
  ngf_descriptor_set_bind_op descriptor_set_bind_op = {
    .set = app_state.uniform_buffer_ds.get(),
    .slot = 0u
  };
  ngf_draw_subop_info ds_bind_subop_info = {
    .dynamic_state_cmds = nullptr,
    .ndynamic_state_cmds = 0u,
    .ndescriptor_set_bind_ops = 1u,
    .descriptor_set_bind_ops = &descriptor_set_bind_op,
    .vertex_buf_bind_ops = nullptr,
    .nvertex_buf_bind_ops = 0u,
    .mode = NGF_DRAW_MODE_DIRECT,
    .index_buf_bind_op = nullptr,
    .primitive = NGF_PRIMITIVE_TYPE_TRIANGLE_LIST,
    .first_element = 0u,
    .nelements = 0u,
    .ninstances = 0u
  };
  ngf_draw_op_info ds_bind_op_info = {
    .pipeline = app_state.pipeline.get(),
    .subops = &ds_bind_subop_info,
    .nsubops = 1u
  };
  ngf_owned_draw_op ds_bind_op;
  ngf_err = ds_bind_op.initialize(&ds_bind_op_info);
  assert(ngf_err == NGF_ERROR_OK);
  
  ngf_render_target *rt;
  ngf_default_render_target(&rt);

  ngf_draw_op *draw_op = ds_bind_op.get();
  ngf_err = ngf_execute_pass(app_state.renderpass.get(), rt, &draw_op, 1u);
  assert(ngf_err == NGF_ERROR_OK);

  glm::mat4 projection = glm::infinitePerspective(45.0f, 4.0f/3.0f, 0.01f);
  glm::mat4 view = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, -5.0f));

  uniform_buffer ub;
  ub.view_projection = view * projection;
  ngf_populate_buffer(app_state.uniform_buffer.get(),
                      0, sizeof(ub), (void*)&ub);
  while (!glfwWindowShouldClose(win)) {
    ngf_begin_frame(app_state.context.get());
    glfwPollEvents();
    draw_op = app_state.draw_op.get();
    ngf_execute_pass(app_state.renderpass.get(), rt, &draw_op, 1u);
    ngf_end_frame(app_state.context.get());
  }
  return 0;
}

