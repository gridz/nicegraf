from conans import ConanFile, CMake, tools


class NicegrafConan(ConanFile):
    name = "nicegraf"
    version = "0.1"
    license = "MIT"
    url = "https://bitbucket.org/gridz/nicegraf"
    description = "An abstraction layer for low-level platform-specific graphics APIs."
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    exports_sources = "*"
    os = ["windows", "linux"]

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder=".", build_folder=".")
        cmake.build(target="nicegraf")
        cmake.build(target="nicegraf_util")
        cmake.build(target="nicegraf_shaderc")

    def package(self):
        self.copy("*.h", dst="include", src="nicegraf")
        self.copy("*nicegraf.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["nicegraf"]

