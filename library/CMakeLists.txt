cmake_minimum_required(VERSION 3.0.2)
project(nicegraf)

set(SHADERC_SKIP_TESTS "on")
set(SPIRV_SKIP_TESTS "on")

set(NGF_PLATFORM "GL")

set(NICEGRAF_SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/include/nicegraf.h
    ${CMAKE_CURRENT_LIST_DIR}/source/nicegraf_internal.h
    ${CMAKE_CURRENT_LIST_DIR}/source/nicegraf_internal.c)

set(EGL_NO_GLEW ON)

if("${NGF_PLATFORM}" STREQUAL "GL")
  set(NICEGRAF_SOURCES
    ${NICEGRAF_SOURCES}
    ${CMAKE_CURRENT_LIST_DIR}/source/nicegraf_impl_gl43.c
    ${CMAKE_CURRENT_LIST_DIR}/source/gl_43_core.c)
  add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/../third_party/EGL/EGL ./EGL-binaries)
  set(NICEGRAF_DEPS
      ${NICEGRAF_DEPS}
      egl)
elseif("${NGF_PLATFORM}" STREQUAL "VK")
  find_package(Vulkan REQUIRED)
  get_property(VK_INCLUDE_PATH TARGET Vulkan::Vulkan PROPERTY INTERFACE_INCLUDE_DIRECTORIES)
  add_definitions("-DVMA_STATIC_VULKAN_FUNCTIONS=0")
  set(NICEGRAF_SOURCES
    ${NICEGRAF_SOURCES}
    ${CMAKE_CURRENT_LIST_DIR}/source/nicegraf_impl_vk.c)
  add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/../third_party/volk ./volk-binaries)
  add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/../third_party/vma ./vma-binaries)
  set(NICEGRAF_DEPS
      ${NICEGRAF_DEPS}
      volk
      vma)
  set(NICEGRAF_PRIVATE_INCLUDES ${VK_INCLUDE_PATH})
endif()

set(NICEGRAF_UTIL_SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/include/nicegraf_util.h
    ${CMAKE_CURRENT_LIST_DIR}/source/nicegraf_internal.c
    ${CMAKE_CURRENT_LIST_DIR}/source/nicegraf_util.c)

add_library (nicegraf
    ${NICEGRAF_SOURCES})
add_library (nicegraf_util
    ${NICEGRAF_UTIL_SOURCES})
target_include_directories(nicegraf PUBLIC
    ${CMAKE_CURRENT_LIST_DIR}/include)
target_include_directories(nicegraf PRIVATE
    ${CMAKE_CURRENT_LIST_DIR}/source
	${NICEGRAF_PRIVATE_INCLUDES})
target_include_directories(nicegraf_util PUBLIC
    ${CMAKE_CURRENT_LIST_DIR}/include)
target_link_libraries(nicegraf ${NICEGRAF_DEPS})
target_link_libraries(nicegraf_util nicegraf)

