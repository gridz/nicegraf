#pragma once

#include "nicegraf.h"
#include <utility>

/**
 * A move-only RAII wrapper over Nicegraf objects that provides
 * unique ownership semantics
 */ 
template <class T, class ObjectManagementFuncs>
class ngf_handle {
public:
  ngf_handle() : handle_(nullptr) {}
  ngf_handle(const ngf_handle&) = delete;
  ngf_handle(ngf_handle &&other) : handle_(nullptr) {
    *this = std::move(other);
  }
  ~ngf_handle() { destroy_if_necessary(); }

  ngf_handle& operator=(const ngf_handle&) = delete;
  ngf_handle& operator=(ngf_handle&& other) {
    destroy_if_necessary();
    handle_ = other.handle_;
    other.handle_ = nullptr;
  }

  ngf_error initialize(const typename ObjectManagementFuncs::InitType& info) {
    destroy_if_necessary();
    return ObjectManagementFuncs::create(&info, &handle_);
  }

  T* get() { return handle_; }
  operator T*() { return handle_; }

  void reset(T *new_handle) { destroy_if_necessary(); handle_ = new_handle; }

private:
  void destroy_if_necessary() {
    if(handle_) {
      ObjectManagementFuncs::destroy(handle_);
      handle_ = nullptr;
    }
  }

  T *handle_;
};

#define NGF_DEFINE_WRAPPER_TYPE(name) \
  struct ngf_##name##_ManagementFuncs { \
    using InitType = ngf_##name##_info; \
    static ngf_error create(const InitType *info, ngf_##name **r) { \
      return ngf_create_##name(info, r); \
    } \
    static void destroy(ngf_##name *handle) { ngf_destroy_##name(handle); } \
  }; \
  using ngf_owned_##name = ngf_handle<ngf_##name, ngf_##name##_ManagementFuncs>;

NGF_DEFINE_WRAPPER_TYPE(shader_stage);
NGF_DEFINE_WRAPPER_TYPE(descriptors_layout);
NGF_DEFINE_WRAPPER_TYPE(pipeline_layout);
NGF_DEFINE_WRAPPER_TYPE(graphics_pipeline);
NGF_DEFINE_WRAPPER_TYPE(image);
NGF_DEFINE_WRAPPER_TYPE(sampler);
NGF_DEFINE_WRAPPER_TYPE(render_target);
NGF_DEFINE_WRAPPER_TYPE(buffer);
NGF_DEFINE_WRAPPER_TYPE(draw_op);
NGF_DEFINE_WRAPPER_TYPE(pass);
NGF_DEFINE_WRAPPER_TYPE(context);

// special case for descriptor sets
struct ngf_descriptor_set_ManagementFuncs {
  using InitType = ngf_descriptors_layout;
  static ngf_error create(const InitType *layout, ngf_descriptor_set **set) {
    return ngf_create_descriptor_set(layout, set);
  }
  static void destroy(ngf_descriptor_set *handle) {
    ngf_destroy_descriptor_set(handle);
  }
};
using ngf_owned_descriptor_set = ngf_handle<ngf_descriptor_set,
                                            ngf_descriptor_set_ManagementFuncs>;
